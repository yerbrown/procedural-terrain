﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "MapPreset", menuName = "ScriptableObjects/mapPresetScriptableObject", order = 3)]
public class MapPreset : ScriptableObject
{
    public bool unlocked;
    [Range(1, maxLevel)]
    public int minLvl, maxLvl;
    public const int maxLevel = 10;
    public List<CreatureStats> enemies = new List<CreatureStats>();

    public MapPreset nextMap;


    private void OnValidate()
    {
        if (minLvl>maxLvl)
        {
            maxLvl = minLvl;
        }
    }
}
