﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Player 
{
    public string UserId;
    public string UserName;
    public int Score;

    public Player() { }

    public Player(string id, int score, string name)
    {
        UserId = id;
        UserName = name;
        Score = score;
    }
}
