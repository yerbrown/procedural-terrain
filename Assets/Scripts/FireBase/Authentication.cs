﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Firebase.Auth;

public class Authentication : MonoBehaviour
{
    public InputField emailInputLogin;
    public InputField passwordInputLogin;

    public InputField emailInput;
    public InputField passwordInput;
    public InputField usernameInput;
    public Text usernameText;
    FirebaseAuth auth;
    FirebaseUser user;




    private void Start()
    {
        if (FirebaseAuth.DefaultInstance.CurrentUser != null)
        {
            Debug.Log("User logged in: " + FirebaseAuth.DefaultInstance.CurrentUser.Email);
            if (usernameText != null)
            {
                usernameText.text = "User: " + FirebaseAuth.DefaultInstance.CurrentUser.DisplayName + " " + FirebaseAuth.DefaultInstance.CurrentUser.UserId;
            }
        }
    }

    public void Login()
    {
        FirebaseAuth.DefaultInstance.SignInWithEmailAndPasswordAsync(emailInputLogin.text, passwordInputLogin.text).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Firebase.FirebaseException exception = task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;

                GetErrorMessage((AuthError)exception.ErrorCode);

                return;
            }

            if (task.IsFaulted)
            {
                Firebase.FirebaseException exception = task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;

                GetErrorMessage((AuthError)exception.ErrorCode);

                return;
            }

            if (task.IsCompleted)
            {
                //usernameText.text = "User email: " + FirebaseAuth.DefaultInstance.CurrentUser.Email;
                UpdateData();
                Debug.Log("User logged in: " + FirebaseAuth.DefaultInstance.CurrentUser.Email);
                SceneManager.LoadScene(0);
            }
        });
    }

    public void LoginAnonymous()
    {
        FirebaseAuth.DefaultInstance.SignInAnonymouslyAsync().ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Firebase.FirebaseException exception = task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;

                GetErrorMessage((AuthError)exception.ErrorCode);

                return;
            }

            if (task.IsFaulted)
            {
                Firebase.FirebaseException exception = task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;

                GetErrorMessage((AuthError)exception.ErrorCode);

                return;
            }

            if (task.IsCompleted)
            {

                Debug.Log("User logged in");
            }

        });
    }

    public void Logout()
    {
        if (FirebaseAuth.DefaultInstance.CurrentUser != null)
        {
            FirebaseAuth.DefaultInstance.SignOut();
            Debug.Log("User logged out");
            usernameText.text = "No one logged";
        }
    }

    public void ReturnToLogin()
    {
        SceneManager.LoadScene(2);
    }

    public void RegisterUser()
    {
        //To avoid registering something empty, we double check before registering
        if (emailInput.text == "")
        {
            Debug.Log("Enter an email to register");
            return;
        }
        else if (passwordInput.text == "")
        {
            Debug.Log("Enter a valid password to register");
            return;
        }
        else if (usernameInput.text == "")
        {
            Debug.Log("Enter a valid username to register");
            return;
        }

        FirebaseAuth.DefaultInstance.CreateUserWithEmailAndPasswordAsync(emailInput.text, passwordInput.text).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Firebase.FirebaseException exception = task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;

                GetErrorMessage((AuthError)exception.ErrorCode);

                return;
            }

            if (task.IsFaulted)
            {
                Firebase.FirebaseException exception = task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;

                GetErrorMessage((AuthError)exception.ErrorCode);

                return;
            }

            if (task.IsCompleted)
            {
                Debug.Log("User registered");
                ChangeDisplayName();
                PlayersDataSave.playersDataSaveInstance.SaveNewData();
                SceneManager.LoadScene(0);
            }
        });
    }

    private void GetErrorMessage(AuthError errorCode)
    {
        string message = errorCode.ToString();

        switch (errorCode)
        {
            case AuthError.AccountExistsWithDifferentCredentials:
                break;
            case AuthError.MissingPassword:
                break;
            case AuthError.WrongPassword:
                break;
            case AuthError.InvalidEmail:
                break;
        }

        Debug.Log(message);
    }


    public void UpdateData()
    {
        user = FirebaseAuth.DefaultInstance.CurrentUser;
        Debug.Log(user.DisplayName);
        Debug.Log(user.Email);
        Debug.Log(user.UserId);

    }


    public void ChangeDisplayName()
    {
        auth = FirebaseAuth.DefaultInstance;
        user = auth.CurrentUser;
        if (user != null)
        {
            UserProfile profile = new UserProfile
            {
                DisplayName = usernameInput.text
            };
            user.UpdateUserProfileAsync(profile).ContinueWith(userTask =>
            {
                if (userTask.IsCanceled)
                {
                    Debug.LogError("UpdateUserProfileAsync was canceled.");
                    return;
                }
                if (userTask.IsFaulted)
                {
                    Debug.LogError("UpdateUserProfileAsync encountered an error: " + userTask.Exception);
                    return;
                }

                Debug.Log("User profile updated successfully: " + FirebaseAuth.DefaultInstance.CurrentUser.DisplayName);
            });
        }
    }
}
