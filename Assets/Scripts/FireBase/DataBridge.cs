﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Database;
using Firebase.Auth;
using Firebase;


public class DataBridge : MonoBehaviour
{
    public InputField userNameInput;
    public InputField scoreInput;

    private DatabaseReference DBReference;
    private int savedUserScore = 0;
    private int newScore = 0;

    //This is another script created by us. We must create it unles we want an error when declaring the variable
    private Player data = new Player();




    // Start is called before the first frame update
    private async void Start()
    {
        await FirebaseApp.CheckAndFixDependenciesAsync();

        DBReference = FirebaseDatabase.DefaultInstance.RootReference;
        GetCurrentUserScore();
        DBReference.Child("User").Child(FirebaseAuth.DefaultInstance.CurrentUser.UserId).ValueChanged += HandleScoreChange;
    }
    public void SaveData()
    {
        data.UserName = userNameInput.text;
        data.Score = int.Parse(scoreInput.text);
        data.UserId = FirebaseAuth.DefaultInstance.CurrentUser.UserId;

        string json = JsonUtility.ToJson(data);

        DBReference.Child("User").Child(data.UserId).SetRawJsonValueAsync(json).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Firebase.FirebaseException exception = task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;

                GetErrorMessage((AuthError)exception.ErrorCode);

                return;
            }

            if (task.IsFaulted)
            {
                Firebase.FirebaseException exception = task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;

                GetErrorMessage((AuthError)exception.ErrorCode);

                return;
            }

            if (task.IsCompleted)
            {
                //usernameText.text = "User email: " + FirebaseAuth.DefaultInstance.CurrentUser.Email;
                Debug.Log("Data succesfully saved");
            }
        });
    }

    public void LoadData()
    {
        //Option 1: Get all the data and manage it
        //    DBReference.Child("User").GetValueAsync().ContinueWith(task =>
        //    {
        //        if (task.IsCompleted)
        //        {
        //            Debug.Log("Loading succesful");
        //            DataSnapshot snapshot = task.Result;
        //            foreach (var child int snapshot.Children)
        //            {
        //                  string playerToManage = child.GetRawJsonValue();
        //                  Player extractedPlayer = JsonUtility.FromJson<PLayer>(playerToManage);
        //                  Debug.Log(extractedPlayer.UserName);
        //                  Debug.Log(extractedPlayer.Score);
        //            }
        //        }
        //    });
        //Split the snapshot based on the name of the variables we used to create the JSON
        DBReference.Child("User").Child(FirebaseAuth.DefaultInstance.CurrentUser.UserId).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                Debug.Log("Loading successful");
                DataSnapshot snapshot = task.Result;

                Debug.Log(snapshot.Child("UserName").Value.ToString() + " " + snapshot.Child("Score").Value.ToString());
            }
        });
    
    }

    private void GetErrorMessage(AuthError errorCode)
    {
        string message = errorCode.ToString();

        Debug.Log(message);
    }

    private void GetCurrentUserScore()

    {
        DBReference.Child("User").Child(FirebaseAuth.DefaultInstance.CurrentUser.UserId).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                Debug.Log("Current user score loaded");
                DataSnapshot snapshot = task.Result;

                savedUserScore = int.Parse(snapshot.Child("Score").Value.ToString());
            }
        });
    }

    public void SaveIfScoreIsHigher()
    {
        newScore = int.Parse(scoreInput.text);
        if (newScore>savedUserScore)
        {
            SaveData();
        }
    }

    private void HandleScoreChange(object sender, ValueChangedEventArgs args)
    {
        Dictionary<string, object> update = (Dictionary<string, object>)args.Snapshot.Value;
        savedUserScore = int.Parse(update["Score"].ToString());

        //Also if we have any event for when the score is changed, we would call it here
    }
}
