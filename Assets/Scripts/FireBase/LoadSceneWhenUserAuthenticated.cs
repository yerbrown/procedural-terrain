﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Auth;
using UnityEngine.SceneManagement;
using System;
public class LoadSceneWhenUserAuthenticated : MonoBehaviour
{

    [SerializeField]private string sceneToLoad;
    private void Start()
    {
        FirebaseAuth.DefaultInstance.StateChanged += HandleAuthStateChanged;
    }

    private void OnDestroy()
    {
        FirebaseAuth.DefaultInstance.StateChanged -= HandleAuthStateChanged;
    }


    public void HandleAuthStateChanged(object sender, EventArgs e)
    {
        CheckUser();
    }


    public void CheckUser()
    {
        if (FirebaseAuth.DefaultInstance.CurrentUser != null)
        {
            SceneManager.LoadScene(sceneToLoad);
        }
    }
}


