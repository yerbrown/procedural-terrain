﻿using UnityEngine;
using UnityEngine.UI;
using Firebase.Auth;
using System;

public class CurrentUserlabel : MonoBehaviour
{
    [SerializeField] private Text userLabel;

    private void Reset()
    {
        if (userLabel == null)
        {
            userLabel = GetComponent<Text>();
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        FirebaseAuth.DefaultInstance.StateChanged += HandleAuthStateChanged;
        UpdateUserLabel();
    }

    private void OnDestroy()
    {
        FirebaseAuth.DefaultInstance.StateChanged -= HandleAuthStateChanged;
    }

    public void HandleAuthStateChanged(object sender, EventArgs e)
    {
        UpdateUserLabel();
    }

    public void UpdateUserLabel()
    {
        var currentUser = FirebaseAuth.DefaultInstance.CurrentUser;
        string userName = "Placeholder";

        if (currentUser == null)
        {
            userName = "No user";
        }
        else
        {
            //We can either show the user Id (which will be unique for each user)...

            userName = currentUser.UserId + " : " + currentUser.DisplayName;
            //...or we can show the current user email
            //userName = currentUser.Email;
        }
        userLabel.text = userName;
    }
}
