﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase.Auth;
using UnityEngine.SceneManagement;

public class LogoutButton : MonoBehaviour
{
    public void LogOut()
    {
        if (FirebaseAuth.DefaultInstance.CurrentUser != null)
        {
            FirebaseAuth.DefaultInstance.SignOut();
            Debug.Log("User logged out");
            SceneManager.LoadScene(0);
        }
    }
}
