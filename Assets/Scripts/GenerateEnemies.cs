﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateEnemies : MonoBehaviour
{
    public static GenerateEnemies generateEnemiesInstance;
    public GenerateTerrainManager terrainManager;
    public GameObject creaturePrefab;

    public List<GameObject> allEnemiesInScene = new List<GameObject>();

    public MapPreset currentMap;
    private void Awake()
    {
        generateEnemiesInstance = this;
        terrainManager = GenerateTerrainManager.generateTerrainManagerInstance;
    }
    private void Start()
    {
        currentMap = InventoryAndCreatureManager.InventoryAndCreature.selectedMap;
        GenerateEnemiesInRows();
    }

    public void DestroyAllEnemies()
    {
        for (int i = allEnemiesInScene.Count - 1; i >= 0; i--)
        {
            Destroy(allEnemiesInScene[i].gameObject);
        }
        allEnemiesInScene.Clear();
    }

    public void GenerateEnemiesInRows()
    {
        DestroyAllEnemies();
        for (int i = 0; i < 4; i++)
        {
            int row = i;

            int randomEnemy = Random.Range(0, currentMap.enemies.Count);
            GameObject newCreature = Instantiate(creaturePrefab, terrainManager.allBlocksByRow[row].Blocks[terrainManager.allBlocksByRow[row].Blocks.Count - 1].GetComponent<BlockId>().nextPos.position, Quaternion.Euler(0, 0, 0));
            allEnemiesInScene.Add(newCreature);
            newCreature.GetComponent<SpriteRenderer>().sortingOrder = (5 * row) - 1;
            newCreature.GetComponent<SpriteRenderer>().flipX = false;
            newCreature.GetComponent<CreatureUIController>().enemy = true;
            newCreature.GetComponent<CreatureExperienceController>().currentLvl = Random.Range(currentMap.minLvl, currentMap.maxLvl + 1);
            newCreature.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = (5 * row) - 1;
            newCreature.transform.GetChild(1).GetComponent<Canvas>().sortingOrder = (5 * row) - 1;
            CharactersControllers characterScript = newCreature.GetComponent<CharactersControllers>();
            characterScript.creatureInfo = currentMap.enemies[Random.Range(0, currentMap.enemies.Count)];
            newCreature.GetComponent<SpriteRenderer>().sprite = newCreature.GetComponent<CharactersControllers>().creatureInfo.creaturePlayImg;
            characterScript.enemy = true;

            for (int j = terrainManager.allBlocksByRow[row].Blocks.Count - 1; j >= 0; j--)
            {
                if (terrainManager.allBlocksByRow[row].Blocks[j].transform.childCount > 1)
                {
                    if (terrainManager.allBlocksByRow[row].Blocks[j].transform.GetChild(terrainManager.allBlocksByRow[row].Blocks[j].transform.childCount - 1).GetComponent<BlockId>() != null)
                    {
                        characterScript.path.Add(new CharactersControllers.TerrainBlock(terrainManager.allBlocksByRow[row].Blocks[j].transform.GetChild(terrainManager.allBlocksByRow[row].Blocks[j].transform.childCount - 1).GetComponent<BlockId>().blockId, terrainManager.allBlocksByRow[row].Blocks[j].transform.GetChild(terrainManager.allBlocksByRow[row].Blocks[j].transform.childCount - 1).GetComponent<BlockId>().nextPosE.position));
                    }
                    else
                    {
                        characterScript.path.Add(new CharactersControllers.TerrainBlock(terrainManager.allBlocksByRow[row].Blocks[j].GetComponent<BlockId>().blockId, terrainManager.allBlocksByRow[row].Blocks[j].GetComponent<BlockId>().nextPosE.position, terrainManager.allBlocksByRow[row].Blocks[j].transform.GetChild(0).GetComponent<Obstacle>()));
                    }
                }
                else
                {
                    characterScript.path.Add(new CharactersControllers.TerrainBlock(terrainManager.allBlocksByRow[row].Blocks[j].GetComponent<BlockId>().blockId, terrainManager.allBlocksByRow[row].Blocks[j].GetComponent<BlockId>().nextPosE.position));
                }

                if (j == terrainManager.allBlocksByRow[row].Blocks.Count - 1)
                {
                    Vector2 firstPos = Vector2.zero;
                    if (terrainManager.allBlocksByRow[row].Blocks[j].transform.childCount > 1)
                    {
                        if (terrainManager.allBlocksByRow[row].Blocks[j].transform.GetChild(terrainManager.allBlocksByRow[row].Blocks[j].transform.childCount - 1).GetComponent<BlockId>() != null)
                        {
                            firstPos = terrainManager.allBlocksByRow[row].Blocks[j].transform.GetChild(terrainManager.allBlocksByRow[row].Blocks[j].transform.childCount - 1).GetComponent<BlockId>().nextPos.position;
                            newCreature.transform.position = new Vector2(firstPos.x - 0.5f, firstPos.y + (terrainManager.allBlocksByRow[row].Blocks[j].transform.GetChild(terrainManager.allBlocksByRow[row].Blocks[j].transform.childCount - 1).GetComponent<BlockId>().nextPosE.position.y - firstPos.y) / 2);
                        }
                        else
                        {
                            firstPos = terrainManager.allBlocksByRow[row].Blocks[j].GetComponent<BlockId>().nextPos.position;
                        }
                    }
                    else
                    {
                        firstPos = terrainManager.allBlocksByRow[row].Blocks[j].GetComponent<BlockId>().nextPos.position;
                        newCreature.transform.position = new Vector2(firstPos.x - 0.5f, firstPos.y + (terrainManager.allBlocksByRow[row].Blocks[terrainManager.allBlocksByRow[row].Blocks.Count - 1].GetComponent<BlockId>().nextPosE.position.y - firstPos.y) / 2);
                    }

                }
            }
        }
    }
    public void RemoveFromCurrentScene(GameObject currentEnemy)
    {
        allEnemiesInScene.Remove(currentEnemy);
        Destroy(currentEnemy);
    }

    public bool CompareRowEmpty(int row)
    {
        for (int i = 0; i < allEnemiesInScene.Count; i++)
        {
            if (allEnemiesInScene[i].GetComponent<CharactersControllers>().path[allEnemiesInScene[i].GetComponent<CharactersControllers>().path.Count - 1].nextPos.y == terrainManager.allBlocksByRow[row].Blocks[0].GetComponent<BlockId>().nextPosE.position.y)
            {
                return false;
            }
        }
        return true;
    }

    public void StartPlay()
    {
        for (int i = 0; i < allEnemiesInScene.Count; i++)
        {
            allEnemiesInScene[i].GetComponent<CharactersControllers>().inPlay = true;
        }
    }
}
