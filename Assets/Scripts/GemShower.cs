﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class GemShower : MonoBehaviour
{
    public static GemShower gemShowerInstance;
    public Text gem1Txt, gem2Txt, gem3Txt;
    public Inventory inventory;

    private void Awake()
    {
        gemShowerInstance = this;
    }

    private void Start()
    {
        SearchForGems();
    }

    public void SearchForGems()
    {
        if (inventory.gem1 < 0)
        {
            Invoke(nameof(SearchForGems), 0.2f  );
        }
        else
        {
            ShowGemsAmount();
        }
    }
    public void ShowGemsAmount()
    {
        gem1Txt.text = inventory.gem1.ToString("00");
        gem2Txt.text = inventory.gem2.ToString("00");
        gem3Txt.text = inventory.gem3.ToString("00");
    }
}
