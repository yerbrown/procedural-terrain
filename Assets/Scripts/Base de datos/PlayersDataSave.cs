﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase.Database;
using Firebase.Auth;
using Firebase;
using UnityEngine.Events;
using System.Threading.Tasks;

public class PlayersDataSave : MonoBehaviour
{
    public static PlayersDataSave playersDataSaveInstance;

    public Inventory inventory;

    private DatabaseReference DBReference;
    public string UserGems;
    public string UserLastMapUnlocked;

    private PlayerData data = new PlayerData();
    [HideInInspector]
    public UnityEvent updateGems;

    private void Awake()
    {

        if (PlayersDataSave.playersDataSaveInstance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            playersDataSaveInstance = this;
        }
    }

    private async void Start()
    {
        await FirebaseApp.CheckAndFixDependenciesAsync();

        DBReference = FirebaseDatabase.DefaultInstance.RootReference;
        if (FirebaseAuth.DefaultInstance.CurrentUser != null)
        {
            await GetCurrentUserInfo();
            GemShower.gemShowerInstance.ShowGemsAmount();
            LoadCreaturesData();
        }
        //DBReference.Child("User").Child(FirebaseAuth.DefaultInstance.CurrentUser.UserId).ValueChanged += HandleScoreChange;
    }

    public void TransformToString()
    {
        UserGems = inventory.gem1 + "," + inventory.gem2 + "," + inventory.gem3;
    }
    public void SaveData()
    {
        TransformToString();
        data.UserId = FirebaseAuth.DefaultInstance.CurrentUser.UserId;
        data.UserName = FirebaseAuth.DefaultInstance.CurrentUser.DisplayName;
        data.Gems = UserGems;
        UserLastMapUnlocked = inventory.lastMap;
        data.LastMapUnlocked = UserLastMapUnlocked;

        string json = JsonUtility.ToJson(data);

        DBReference.Child("User").Child(data.UserId).SetRawJsonValueAsync(json).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Firebase.FirebaseException exception = task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;

                GetErrorMessage((AuthError)exception.ErrorCode);

                return;
            }

            if (task.IsFaulted)
            {
                Firebase.FirebaseException exception = task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;

                GetErrorMessage((AuthError)exception.ErrorCode);

                return;
            }

            if (task.IsCompleted)
            {
                //usernameText.text = "User email: " + FirebaseAuth.DefaultInstance.CurrentUser.Email;
                Debug.Log("Data succesfully saved");
            }
        });



    }


    public void SaveNewData()
    {
        TransformToString();
        data.UserId = FirebaseAuth.DefaultInstance.CurrentUser.UserId;
        data.UserName = FirebaseAuth.DefaultInstance.CurrentUser.DisplayName;
        data.Gems = "0,0,0";
        UserLastMapUnlocked = "1-1";
        data.LastMapUnlocked = UserLastMapUnlocked;

        string json = JsonUtility.ToJson(data);

        DBReference.Child("User").Child(data.UserId).SetRawJsonValueAsync(json).ContinueWith(task =>
        {
            if (task.IsCanceled)
            {
                Firebase.FirebaseException exception = task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;

                GetErrorMessage((AuthError)exception.ErrorCode);

                return;
            }

            if (task.IsFaulted)
            {
                Firebase.FirebaseException exception = task.Exception.Flatten().InnerExceptions[0] as Firebase.FirebaseException;

                GetErrorMessage((AuthError)exception.ErrorCode);

                return;
            }

            if (task.IsCompleted)
            {
                //usernameText.text = "User email: " + FirebaseAuth.DefaultInstance.CurrentUser.Email;
                Debug.Log("Data succesfully saved");
            }
        });



    }
    public void UpdateCreaturesInUser(List<CreatureData.CreatureInfo> allMycreatures)
    {
        CreatureData.CreatureInfo newCreature = new CreatureData.CreatureInfo();
        data.UserId = FirebaseAuth.DefaultInstance.CurrentUser.UserId;
        for (int i = 0; i < allMycreatures.Count; i++)
        {
            newCreature = allMycreatures[i];
            string json = JsonUtility.ToJson(newCreature);

            DBReference.Child("User").Child(data.UserId).Child("Creatures").Child(newCreature.Name).SetRawJsonValueAsync(json).ContinueWith(task =>
            {
                if (task.IsCanceled)
                {
                    FirebaseException exception = task.Exception.Flatten().InnerExceptions[0] as FirebaseException;

                    GetErrorMessage((AuthError)exception.ErrorCode);

                    return;
                }

                if (task.IsFaulted)
                {
                    FirebaseException exception = task.Exception.Flatten().InnerExceptions[0] as FirebaseException;

                    GetErrorMessage((AuthError)exception.ErrorCode);

                    return;
                }

                if (task.IsCompleted)
                {
                    //usernameText.text = "User email: " + FirebaseAuth.DefaultInstance.CurrentUser.Email;
                    Debug.Log(" Creature data succesfully saved");
                }
            });
        }
    }

    public void LoadCreaturesData()
    {
        data.UserId = FirebaseAuth.DefaultInstance.CurrentUser.UserId;
        CreatureData myCreaturesList = GetComponent<CreatureData>();

        DBReference.Child("User").Child(data.UserId).Child("Creatures").GetValueAsync().ContinueWith(task =>
            {
                if (task.IsCompleted)
                {
                    DataSnapshot snapshot = task.Result;

                    foreach (var child in snapshot.Children)
                    {
                        string creatureToManage = child.GetRawJsonValue();
                        CreatureData.CreatureInfo extractedCreature = JsonUtility.FromJson<CreatureData.CreatureInfo>(creatureToManage);
                        myCreaturesList.myCreatures.Add(extractedCreature);
                    }

                    Debug.Log("Loading succesful");
                }
            });
    }



    private async Task GetCurrentUserInfo()

    {
        await DBReference.Child("User").Child(FirebaseAuth.DefaultInstance.CurrentUser.UserId).GetValueAsync().ContinueWith(task =>
        {
            if (task.IsCompleted)
            {
                Debug.Log("Current user info loaded");
                DataSnapshot snapshot = task.Result;

                UserGems = snapshot.Child("Gems").Value.ToString();
                string[] gems = UserGems.Split(char.Parse(","));
                inventory.gem1 = int.Parse(gems[0]);
                inventory.gem2 = int.Parse(gems[1]);
                inventory.gem3 = int.Parse(gems[2]);
                UserLastMapUnlocked = snapshot.Child("LastMapUnlocked").Value.ToString();
                Debug.Log("Loading user data succesful");
                return;
            }
        });
    }

    private void GetErrorMessage(AuthError errorCode)
    {
        string message = errorCode.ToString();

        Debug.Log(message);
    }

    private void HandleScoreChange(object sender, ValueChangedEventArgs args)
    {
        Dictionary<string, object> update = (Dictionary<string, object>)args.Snapshot.Value;
        if (update != null)
        {
            UserGems = update["Gems"].ToString();
        }

        //Also if we have any event for when the score is changed, we would call it here
    }


}
