﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatureData : MonoBehaviour
{
    public PlayersDataSave dataSaveController;
    private int numInList;

    [System.Serializable]
    public class CreatureInfo
    {
        public string KeyID;
        public string Name;
        public int Exp;
        public int Level;
        public bool Unlocked;
    }

    public List<CreatureInfo> myCreatures = new List<CreatureInfo>();

    private void Start()
    {
        dataSaveController = GetComponent<PlayersDataSave>();
        SetCreaturesStats();
    }

    
    public void UpdateMyCreaturesList(CreatureStats creatureStats)
    {
        if (CompareIfCreatureInList(creatureStats))
        {
            myCreatures[numInList].Exp = creatureStats.currentExp;
            myCreatures[numInList].Level = creatureStats.level;
        }
        else
        {
            myCreatures.Add(new CreatureInfo());
            myCreatures[myCreatures.Count - 1].KeyID = creatureStats.keyID;
            myCreatures[myCreatures.Count - 1].Name = creatureStats.creatureName;
            myCreatures[myCreatures.Count - 1].Exp = creatureStats.currentExp;
            myCreatures[myCreatures.Count - 1].Level = creatureStats.level;
            myCreatures[myCreatures.Count - 1].Unlocked = creatureStats.unlocked;
        }

    }

    public bool CompareIfCreatureInList(CreatureStats creature)
    {
        for (int i = 0; i < myCreatures.Count; i++)
        {
            if (creature.keyID == myCreatures[i].KeyID)
            {
                numInList = i;
                return true;
            }
        }
        return false;
    }

    public void SetCreaturesStats()
    {
        for (int i = 0; i < myCreatures.Count; i++)
        {
            for (int j = 0; j < dataSaveController.inventory.allcreatures.Count; j++)
            {
                if (myCreatures[i].KeyID == dataSaveController.inventory.allcreatures[j].keyID)
                {
                    dataSaveController.inventory.allcreatures[j].LevelUp(myCreatures[i].Level);
                    dataSaveController.inventory.allcreatures[j].currentExp = myCreatures[i].Exp;
                    dataSaveController.inventory.allcreatures[j].unlocked = myCreatures[i].Unlocked;
                }
            }
        }
    }

    public void ResetCreatures()
    {
        myCreatures.Clear();
        Destroy(gameObject);
    }
}
