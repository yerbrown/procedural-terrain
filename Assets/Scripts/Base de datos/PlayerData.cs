﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerData 
{
    public string UserId;
    public string UserName;
    public string Gems = "0,0,0";
    public string LastMapUnlocked;
}
