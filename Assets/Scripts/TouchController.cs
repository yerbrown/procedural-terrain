﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchController : MonoBehaviour
{
    public GameObject currenCreature;


    // Update is called once per frame
    void Update()
    {
#if UNITY_IOS
        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position), Vector2.zero);
                if (hit.collider != null)
                {
                    if (hit.collider.CompareTag("Creature"))
                    {
                        hit.collider.GetComponent<CreatureDragAndDrop>().OnTouchDown();
                        currenCreature = hit.collider.gameObject;
                    }
                }
            }
            else if (currenCreature != null && Input.GetTouch(0).phase == TouchPhase.Ended)
            {
                currenCreature.GetComponent<CreatureDragAndDrop>().OnTouchUp();
                currenCreature = null;
            }

        }
#endif
    }
}
