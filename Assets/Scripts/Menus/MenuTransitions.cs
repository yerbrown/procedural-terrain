﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuTransitions : MonoBehaviour
{
    public GameObject currentMenu;
    public Animator anim;

    public GameObject mapMenu;
    public GameObject currentWindow, backButton;
    public bool started;


    private void Start()
    {
        if (PlayerPrefs.GetInt("Gameplay", 0) == 1)
        {
            mapMenu.SetActive(true);
            currentWindow = mapMenu;
            backButton.SetActive(true);
            PlayerPrefs.SetInt("Gameplay", 0);
        }
    }

    public void PlayButton()
    {
        PlayerPrefs.SetInt("Gameplay", 1);
        SceneManager.LoadScene(1);
    }

    public void CreaturesMenu()
    {
        anim.SetBool("Creatures", true);
    }

    public void ReturnToMainMenu()
    {
        anim.SetBool("Creatures", false);
    }

    public void ExitApp()
    {
        Application.Quit();
    }

    public void ExitButton()
    {
        anim.SetBool("Exit", true);
    }

    public void CancelExit()
    {
        anim.SetBool("Exit", false);
    }

    public void BackWindow()
    {
        currentWindow.SetActive(false);
        if (currentWindow.GetComponent<UIWindow>().backWindow != null)
        {
            currentWindow = currentWindow.GetComponent<UIWindow>().backWindow;
            currentWindow.SetActive(true);
        }
        else
        {
            currentWindow = null;
            backButton.SetActive(false);
        }
    }

    public void OpenWindow(GameObject window)
    {
        if (currentWindow != null)
        {
            currentWindow.SetActive(false);
        }
        backButton.SetActive(true);
        window.SetActive(true);
        currentWindow = window;
    }

}
