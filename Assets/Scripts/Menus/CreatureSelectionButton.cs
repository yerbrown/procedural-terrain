﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreatureSelectionButton : MonoBehaviour
{
    public bool selectedCreature;
    public CreatureStats currentCreature;
    public Image creatureImg;


    public void SelectThisCreature()
    {
        if (!selectedCreature)
        {
            selectedCreature = true;
            CreatureSelectorManager.selectorManager.SelectCreature(currentCreature);
            GetComponent<Image>().color = Color.red;
        }
    }
    public void UpdateButton()
    {
        creatureImg.sprite = currentCreature.creatureImg;
    }
}
