﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreaturesSelection : MonoBehaviour
{
    public static CreaturesSelection creaturesSelectionInstance;


    public int selectedCreature;

    public List<GameObject> creaturePrefabs = new List<GameObject>();

    public List<GameObject> creaturesSelected = new List<GameObject>();

    public List<Transform> selectCreatureCuad = new List<Transform>();

    public GameObject startButton;

    public float offsetY;
    private void Awake()
    {
        creaturesSelectionInstance = this;
    }

    private void Start()
    {
        for (int i = 0; i < InventoryAndCreatureManager.InventoryAndCreature.selectedCreatures.Count; i++)
        {
            if (InventoryAndCreatureManager.InventoryAndCreature.selectedCreatures[i]!=null)
            {
                GameObject newCreature = Instantiate(creaturePrefabs[0]);
                creaturesSelected.Add(newCreature);
            }
        }
        for (int i = 0; i < creaturesSelected.Count; i++)
        {
            creaturesSelected[i].GetComponent<CharactersControllers>().creatureInfo = InventoryAndCreatureManager.InventoryAndCreature.selectedCreatures[i];
        }
        AssignStats();
        RestarAllCreaturesPos();
    }

    public void AssignStats()
    {
        for (int i = 0; i < creaturesSelected.Count; i++)
        {
            creaturesSelected[i].GetComponent<CharactersControllers>().attack = creaturesSelected[i].GetComponent<CharactersControllers>().creatureInfo.attack;
            creaturesSelected[i].GetComponent<CharactersControllers>().lifeMax = creaturesSelected[i].GetComponent<CharactersControllers>().creatureInfo.lifeMax;
            creaturesSelected[i].GetComponent<CharactersControllers>().expController.currentExp = creaturesSelected[i].GetComponent<CharactersControllers>().creatureInfo.currentExp;
            creaturesSelected[i].GetComponent<CharactersControllers>().expController.maxExp = creaturesSelected[i].GetComponent<CharactersControllers>().creatureInfo.maxExpLvl;
            creaturesSelected[i].GetComponent<CharactersControllers>().expController.currentLvl = creaturesSelected[i].GetComponent<CharactersControllers>().creatureInfo.level;
        }
    }

    public void RestarAllCreaturesPos()
    {

        for (int i = 0; i < creaturesSelected.Count; i++)
        {
            creaturesSelected[i].transform.position = selectCreatureCuad[i].position + Vector3.down * offsetY;
            creaturesSelected[i].GetComponent<SpriteRenderer>().sortingOrder = 50;
            creaturesSelected[i].GetComponent<CharactersControllers>().inGround = false;
            creaturesSelected[i].GetComponent<CharactersControllers>().speed = 1.5f;
            creaturesSelected[i].GetComponent<CharactersControllers>().inPlay = false;
            creaturesSelected[i].GetComponent<CharactersControllers>().completedPath = false;
            creaturesSelected[i].GetComponent<CharactersControllers>().path.Clear();
            creaturesSelected[i].GetComponent<CharactersControllers>().currentPoint = 0;
            creaturesSelected[i].GetComponent<CharactersControllers>().anim.SetBool("Finish", false);
            creaturesSelected[i].GetComponent<CharactersControllers>().life = creaturesSelected[i].GetComponent<CharactersControllers>().lifeMax;
            creaturesSelected[i].GetComponent<CharactersControllers>().transform.GetChild(1).gameObject.SetActive(false);
        }
        ActivateStartButton();
    }
    public void RestartcreaturePos(GameObject creature)
    {
        for (int i = 0; i < creaturesSelected.Count; i++)
        {
            if (creature == creaturesSelected[i])
            {
                creaturesSelected[i].transform.position = selectCreatureCuad[i].position + Vector3.down * offsetY;
                creaturesSelected[i].GetComponent<SpriteRenderer>().sortingOrder = 50;
                creaturesSelected[i].GetComponent<CharactersControllers>().inGround = false;
            }
        }
    }

    public void ActivateStartButton()
    {
        if (GenerateTerrainManager.generateTerrainManagerInstance.terrainGenerated)
        {

            if (CompareIfAllCreaturesInGround())
            {
                startButton.SetActive(true);
            }
            else
            {

                startButton.SetActive(false);
            }
        }
    }

    public bool CompareIfAllCreaturesInGround()
    {
        for (int i = 0; i < creaturesSelected.Count; i++)
        {
            if (!creaturesSelected[i].GetComponent<CharactersControllers>().inGround && creaturesSelected[i].activeSelf)
            {
                return false;
            }
        }
        return true;
    }

    public bool CompareCreatureInBlock(Vector3 pos)
    {
        for (int i = 0; i < creaturesSelected.Count; i++)
        {
            if (creaturesSelected[i].GetComponent<CharactersControllers>().path.Count > 0 && creaturesSelected[i].GetComponent<CharactersControllers>().path[0].nextPos == pos)
            {
                return false;
            }
        }
        return true;
    }

    public void StartPlay()
    {
        for (int i = 0; i < creaturesSelected.Count; i++)
        {
            creaturesSelected[i].GetComponent<CharactersControllers>().inPlay = true;

        }
    }
}
