﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreatureSelectInfo : MonoBehaviour
{
    public CreatureStats creatureInfo;

    private void Start()
    {
        UpdateButton();
    }

    public void UpdateButton()
    {
        gameObject.transform.GetChild(0).GetComponent<Image>().sprite = creatureInfo.creatureImg;
        if (!creatureInfo.unlocked)
        {
            gameObject.transform.GetChild(0).GetComponent<Image>().color = Color.black;
            GetComponent<Button>().interactable = false;
        }
    }

    public void SelectCreature()
    {
        CreatureInfoViewerController.infoViewer.AssignCreature(creatureInfo);

    }
}
