﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreatureInfoViewerController : MonoBehaviour
{
    public static CreatureInfoViewerController infoViewer;

    public MyCreaturesUnlockController creaturesContainer;
    [Header("UI")]
    public Text creatureName;
    public Text creatureLevel;
    public Image creatureImage, preEvoImg;
    public Text creatureAttack, creatureAttackSpeed;
    public Image attackBar;
    public Text creatureLifeMax;
    public Image lifeBar;
    public Text creatureExp, creatureExpEvo, creatureLevelExp, creatureLevelEvo;
    public Image expBar, expEvoBar, evoImg;
    public Button evolveButon;
    public Text gem1Txt, gem2Txt, gem3Txt;
    public GameObject expWindow, expEvoWindow;

    [Header("Creature")]

    public CreatureStats currentCreature;

    private void Awake()
    {
        infoViewer = this;
    }
    private void Start()
    {
        UpdateInfo();
    }
    public void UpdateInfo()
    {
        creatureName.text = currentCreature.creatureName;
        creatureLevel.text = "Lvl - " + currentCreature.level.ToString("00");
        creatureImage.sprite = currentCreature.creatureMenuImg;
        creatureAttack.text = "Attack: " + currentCreature.attack;
        creatureAttackSpeed.text = "Attack Speed: " + currentCreature.attackRate + " s";
        attackBar.fillAmount = (float)currentCreature.attack / currentCreature.lvlStats[currentCreature.lvlStats.Count - 1].damage;
        creatureLifeMax.text = "Life: " + currentCreature.lifeMax;
        lifeBar.fillAmount = (float)currentCreature.lifeMax / currentCreature.lvlStats[currentCreature.lvlStats.Count - 1].lifemax;
        if (currentCreature.preEvolution!=null)
        {
            preEvoImg.transform.parent.gameObject.SetActive(true);
            preEvoImg.sprite = currentCreature.preEvolution.creatureMenuImg;
        }
        else
        {
            preEvoImg.transform.parent.gameObject.SetActive(false);
        }
        if (currentCreature.evolution != null)
        {
            creatureExpEvo.text = "Exp: " + currentCreature.currentExp + " / " + currentCreature.maxExpLvl;
            creatureLevelEvo.text = "Lvl - " + currentCreature.level.ToString("00") + " / " + currentCreature.lvlStats[currentCreature.lvlStats.Count - 1].lvl.ToString("00");
            expEvoBar.fillAmount = (float)currentCreature.currentExp / currentCreature.maxExpLvl;
            evoImg.sprite = currentCreature.evolution.creatureMenuImg;
            if (currentCreature.evolution.unlocked)
            {
                evoImg.color = Color.white;
            }
            else
            {
                evoImg.color = Color.black;
            }
            expEvoWindow.SetActive(true);
            expWindow.SetActive(false);
            gem1Txt.text = currentCreature.evoGem1.ToString("00");
            gem2Txt.text = currentCreature.evoGem2.ToString("00");
            gem3Txt.text = currentCreature.evoGem3.ToString("00");
            if (currentCreature.level == currentCreature.lvlStats.Count && currentCreature.evoGem1 <= InventoryAndCreatureManager.InventoryAndCreature.myInventory.gem1 && currentCreature.evoGem2 <= InventoryAndCreatureManager.InventoryAndCreature.myInventory.gem2 && currentCreature.evoGem3 <= InventoryAndCreatureManager.InventoryAndCreature.myInventory.gem3)
            {
                if (!currentCreature.evolution.unlocked)
                {
                    evolveButon.interactable = true;
                }
                else
                {
                    evolveButon.interactable = false;
                }
            }
            else
            {
                evolveButon.interactable = false;
            }
        }
        else
        {
            creatureExp.text = "Exp: " + currentCreature.currentExp + " / " + currentCreature.maxExpLvl;
            creatureLevelExp.text = "Lvl - " + currentCreature.level.ToString("00") + " / " + currentCreature.lvlStats[currentCreature.lvlStats.Count - 1].lvl.ToString("00");
            expBar.fillAmount = (float)currentCreature.currentExp / currentCreature.maxExpLvl;
            expEvoWindow.SetActive(false);
            expWindow.SetActive(true);
        }
    }

    public void AssignCreature(CreatureStats selectedCreature)
    {
        currentCreature = selectedCreature;
        UpdateInfo();
    }

    public void EvolveCreature()
    {
        currentCreature.evolution.unlocked = true;
        InventoryAndCreatureManager.InventoryAndCreature.myInventory.gem1 -= currentCreature.evoGem1;
        InventoryAndCreatureManager.InventoryAndCreature.myInventory.gem2 -= currentCreature.evoGem2;
        InventoryAndCreatureManager.InventoryAndCreature.myInventory.gem3 -= currentCreature.evoGem3;
        InventoryAndCreatureManager.InventoryAndCreature.GetComponent<PlayersDataSave>().SaveData();
        InventoryAndCreatureManager.InventoryAndCreature.GetComponent<CreatureData>().UpdateMyCreaturesList(currentCreature.evolution);
        InventoryAndCreatureManager.InventoryAndCreature.GetComponent<PlayersDataSave>().UpdateCreaturesInUser(InventoryAndCreatureManager.InventoryAndCreature.GetComponent<CreatureData>().myCreatures);
        
        UpdateInfo();
        creaturesContainer.CreateLibrary();
        GemShower.gemShowerInstance.ShowGemsAmount();
    }
}
