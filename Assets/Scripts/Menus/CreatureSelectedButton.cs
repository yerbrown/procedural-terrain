﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreatureSelectedButton : MonoBehaviour
{
    public CreatureStats selectedCreature;

    public Image creatureImg;

    public void AssignCreature()
    {
        creatureImg.gameObject.SetActive(true);
        creatureImg.sprite = selectedCreature.creatureImg;
        GetComponent<Button>().interactable = true;
    }

    public void Unassigncreature()
    {
        CreatureSelectorManager.selectorManager.Unselectcreature(selectedCreature);
        selectedCreature = null;
        creatureImg.gameObject.SetActive(false);
        GetComponent<Button>().interactable = false;
    }
}
