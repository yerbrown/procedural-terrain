﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreatureSelectorManager : MonoBehaviour
{
    public static CreatureSelectorManager selectorManager;
    public Inventory allCreaturesInventory;

    public Transform container;

    public List<Button> allCreatures = new List<Button>();
    public List<Button> selectedCreaturesButtons = new List<Button>();

    public GameObject toSelectButtonPrefab;
    public Button startPlay;
    public List<CreatureStats> selectedCreatures = new List<CreatureStats>();

    private void Awake()
    {
        selectorManager = this;
        InventoryAndCreatureManager.InventoryAndCreature.GetComponent<CreatureData>().SetCreaturesStats();
    }
    public void SelectCreature(CreatureStats creature)
    {
        for (int i = 0; i < selectedCreaturesButtons.Count; i++)
        {
            if (selectedCreaturesButtons[i].GetComponent<CreatureSelectedButton>().selectedCreature == null)
            {
                selectedCreaturesButtons[i].GetComponent<CreatureSelectedButton>().selectedCreature = creature;
                selectedCreaturesButtons[i].GetComponent<CreatureSelectedButton>().AssignCreature();
                selectedCreatures[i] = creature;
                DetectSelectedAndDisableOthers();
                ToogleInteractableStartButton();
                return;
            }
        }
    }

    public void ToogleInteractableStartButton()
    {
        for (int i = 0; i < selectedCreatures.Count; i++)
        {
            if (selectedCreatures[i] == null)
            {
                startPlay.interactable = false;
                return;
            }
        }
        startPlay.interactable = true;
    }

    private void OnEnable()
    {
        RemoveAll();
        GenerateAllButtons();
        DetectSelectedAndDisableOthers();
    }



    public void GenerateAllButtons()
    {
        for (int i = 0; i < allCreaturesInventory.allcreatures.Count; i++)
        {
            if (allCreaturesInventory.allcreatures[i].unlocked)
            {
                GameObject newButton = Instantiate(toSelectButtonPrefab, container);
                newButton.GetComponent<CreatureSelectionButton>().currentCreature = allCreaturesInventory.allcreatures[i];
                newButton.GetComponent<CreatureSelectionButton>().UpdateButton();
                allCreatures.Add(newButton.GetComponent<Button>());
            }
        }
    }

    public void RemoveAll()
    {

        for (int i = 0; i < selectedCreaturesButtons.Count; i++)
        {
            selectedCreaturesButtons[i].GetComponent<CreatureSelectedButton>().Unassigncreature();
        }

        for (int i = allCreatures.Count - 1; i >= 0; i--)
        {
            Destroy(allCreatures[i].gameObject);
        }
        allCreatures.Clear();
    }

    public void DetectSelectedAndDisableOthers()
    {
        if (CompareSlectedCreatures())
        {
            for (int i = 0; i < allCreatures.Count; i++)
            {
                if (!allCreatures[i].GetComponent<CreatureSelectionButton>().selectedCreature)
                {
                    allCreatures[i].interactable = false;
                }
            }
        }
        else
        {
            for (int i = 0; i < allCreatures.Count; i++)
            {

                allCreatures[i].interactable = true;
            }
        }
    }

    public bool CompareSlectedCreatures()
    {
        for (int i = 0; i < selectedCreatures.Count; i++)
        {
            if (selectedCreatures[i] == null)
            {
                return false;
            }
        }
        return true;
    }

    public void Unselectcreature(CreatureStats creature)
    {
        for (int i = 0; i < selectedCreatures.Count; i++)
        {
            if (selectedCreatures[i] == creature)
            {
                selectedCreatures[i] = null;
            }
        }

        for (int i = 0; i < allCreatures.Count; i++)
        {
            if (allCreatures[i].GetComponent<CreatureSelectionButton>().currentCreature == creature)
            {
                allCreatures[i].GetComponent<Image>().color = Color.white;
                allCreatures[i].GetComponent<CreatureSelectionButton>().selectedCreature = false;
                DetectSelectedAndDisableOthers();
                ToogleInteractableStartButton();
                return;
            }
        }
    }

    public void TransferSelectedCreatureToPlay()
    {
        for (int i = 0; i < InventoryAndCreatureManager.InventoryAndCreature.selectedCreatures.Count; i++)
        {
            InventoryAndCreatureManager.InventoryAndCreature.selectedCreatures[i] = selectedCreatures[i];

        }
    }
}
