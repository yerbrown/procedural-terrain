﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddGemsBuying : MonoBehaviour
{
    public int gemAmount;
    public int gemType;

    public void AddGems()
    {
        if (gemType == 1)
        {
            PlayersDataSave.playersDataSaveInstance.inventory.gem1 += gemAmount;

        }
        else if (gemType == 2)
        {
            PlayersDataSave.playersDataSaveInstance.inventory.gem2 += gemAmount;

        }
        else
        {
            PlayersDataSave.playersDataSaveInstance.inventory.gem3 += gemAmount;
            
        }
        PlayersDataSave.playersDataSaveInstance.SaveData();
        GemShower.gemShowerInstance.ShowGemsAmount();

    }
}
