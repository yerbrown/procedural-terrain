﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Obstacle : MonoBehaviour
{
    public int life, lifeMax;
    public GameObject lifeCanvas;
    public Image lifeImg;
    public int expGained;
    public bool item;
    public int itemType;
    public void ShowLife(int damage)
    {
        lifeCanvas.SetActive(true);
        life -= damage;
        UpdateUI();
    }

    public void UpdateUI()
    {
        lifeImg.fillAmount = (float)life / lifeMax;
    }
}
