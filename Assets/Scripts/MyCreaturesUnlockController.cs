﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyCreaturesUnlockController : MonoBehaviour
{


    public Inventory allCreatures;
    public List<GameObject> allButtons = new List<GameObject>();
    public GameObject boxPrefab;
    public GameObject panelLibrary;
    private void Start()
    {
        CreateLibrary();
        InventoryAndCreatureManager.InventoryAndCreature.GetComponent<CreatureData>().SetCreaturesStats();
    }


    public void CreateLibrary()
    {
        RemoveAll();
        for (int i = 0; i < allCreatures.allcreatures.Count; i++)
        {
            GameObject newCreature = Instantiate(boxPrefab, panelLibrary.transform);
            newCreature.GetComponent<CreatureSelectInfo>().creatureInfo = allCreatures.allcreatures[i];
            allButtons.Add(newCreature);
        }
    }

    public void RemoveAll()
    {
        for (int i = allButtons.Count - 1; i >= 0; i--)
        {
            Destroy(allButtons[i]);
        }
        allButtons.Clear();
    }
}
