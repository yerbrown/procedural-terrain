﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerateTerrainManager : MonoBehaviour
{
    public static GenerateTerrainManager generateTerrainManagerInstance;

    [System.Serializable]
    public class AllBlocks
    {
        public List<GameObject> Blocks = new List<GameObject>();
    }

    public GameObject groundTerrain;
    public List<GameObject> terrainBlocks = new List<GameObject>();
    public GameObject rampUp, rampDown;

    public GameObject RockStone;
    public List<GameObject> trees = new List<GameObject>();
    public List<GameObject> gems = new List<GameObject>();
    public int columns, rows;
    public Vector2 firstPos;
    public GameObject lastBlock;
    public bool newRow, newColumn;

    public List<AllBlocks> allBlocksByColum = new List<AllBlocks>();
    public List<AllBlocks> allBlocksByRow = new List<AllBlocks>();


    public bool terrainGenerated = false;


    public int percentNext = 75;
    public int percentObstacles = 10;
    public int percentGems = 10, percentGem1, percentGem2, percentGem3;
    public GameObject passLevelWindow;
    private void Awake()
    {
        generateTerrainManagerInstance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        RemoveAll();
        GenerateRowBlocks();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GameMatchManager.gameMatchManagerInstance.PassLevel();
        }
    }

    public void GenerateRowBlocks()
    {
        allBlocksByColum.RemoveAll(allBlocksByColum.Contains);
        allBlocksByRow.RemoveAll(allBlocksByRow.Contains);
        GameObject terrain = new GameObject("Terrain");
        for (int i = 0; i < columns; i++)
        {

            int sameCount = 3;
            bool rampIn = false;
            int rampCount = 2;
            GameObject column = new GameObject("Column " + i);
            column.transform.SetParent(terrain.transform);
            allBlocksByColum.Add(new AllBlocks());
            for (int j = 0; j < rows; j++)
            {
                GameObject newTerrainType = null;

                if (Random.Range(0, 100) >= percentNext && i > 0)
                {

                    newTerrainType = terrainBlocks[allBlocksByColum[i - 1].Blocks[j].GetComponent<BlockId>().blockId];

                }
                else if (i == 0)
                {
                    newTerrainType = terrainBlocks[Random.Range(0, terrainBlocks.Count - 2)];
                }
                else
                {
                    if (sameCount > 0)
                    {
                        newTerrainType = lastBlock;
                        sameCount--;
                    }
                    else if (Random.Range(0, 100) > 70)
                    {
                        int randomFloor = Random.Range(0, terrainBlocks.Count);
                        if (randomFloor==2)
                        {
                            randomFloor = Random.Range(0, 2);
                        }
                        newTerrainType = terrainBlocks[randomFloor];
                        sameCount = 3;
                    }
                    else
                    {
                        newTerrainType = lastBlock;
                    }
                }

                lastBlock = newTerrainType;

                GameObject newBlock = Instantiate(newTerrainType, column.transform);
                newBlock.name = newBlock.name + " Row " + j;
                newBlock.transform.position = new Vector2(firstPos.x + i, firstPos.y - j * 0.75f);
                newBlock.GetComponent<SpriteRenderer>().sortingOrder = 5 * j - 5;
                allBlocksByColum[i].Blocks.Add(newBlock);
            }
        }

        for (int i = 0; i < allBlocksByColum.Count; i++)
        {
            for (int j = 0; j < allBlocksByColum[i].Blocks.Count; j++)
            {
                if (i == 0)
                {
                    allBlocksByRow.Add(new AllBlocks());
                }

                allBlocksByRow[j].Blocks.Add(allBlocksByColum[i].Blocks[j]);
            }
        }
        OrganizeBlocks();
        GenerateRamps();
        AsignBlocktag();

    }

    public void AsignBlocktag()
    {
        for (int i = 0; i < allBlocksByRow.Count; i++)
        {
            allBlocksByRow[i].Blocks[0].tag = "Block";
        }
    }

    public void GenerateRamps()
    {
        for (int i = 0; i < allBlocksByRow.Count; i++)
        {
            bool rampIn = false;
            for (int j = 0; j < allBlocksByRow[i].Blocks.Count; j++)
            {
                if (!rampIn)
                {
                    if (j > 1 && allBlocksByRow[i].Blocks[j].GetComponent<BlockId>().blockId == terrainBlocks[3].GetComponent<BlockId>().blockId && allBlocksByRow[i].Blocks[j - 1].GetComponent<BlockId>().blockId != terrainBlocks[3].GetComponent<BlockId>().blockId && Random.Range(0, 100) > 85)
                    {
                        if (allBlocksByRow[i].Blocks[j - 1].transform.childCount < 2)
                        {
                            GameObject newRamp = Instantiate(rampUp, allBlocksByRow[i].Blocks[j - 1].transform);
                            newRamp.transform.position = new Vector2(firstPos.x + j - 1, firstPos.y - i * 0.75f + 0.45f);
                            newRamp.GetComponent<SpriteRenderer>().sortingOrder = 5 * i - 4;
                            rampIn = true;
                            newRamp = Instantiate(terrainBlocks[2], allBlocksByRow[i].Blocks[j].transform);
                            newRamp.transform.position = new Vector2(firstPos.x + j, firstPos.y - i * 0.75f + 0.45f);
                            newRamp.GetComponent<SpriteRenderer>().sortingOrder = 5 * i - 4;
                        }
                    }
                }
                else
                {
                    GameObject newBlock = null;
                    if (allBlocksByRow[i].Blocks[j].GetComponent<BlockId>().blockId == terrainBlocks[3].GetComponent<BlockId>().blockId)
                    {
                        newBlock = terrainBlocks[2];
                    }
                    else
                    {
                        newBlock = rampDown;
                        rampIn = false;
                    }
                    GameObject newRamp = Instantiate(newBlock, allBlocksByRow[i].Blocks[j].transform);
                    newRamp.transform.position = new Vector2(firstPos.x + j, firstPos.y - i * 0.75f + 0.45f);
                    newRamp.GetComponent<SpriteRenderer>().sortingOrder = 5 * i - 4;
                }
                if (!rampIn && allBlocksByRow[i].Blocks[j].transform.parent.childCount > 1)
                {
                    GenerateObstacles(i, j);
                }
            }
        }
    }

    public void GenerateObstacles(int i, int j)
    {

        if (j > 0 && j < allBlocksByRow[i].Blocks.Count - 1)
        {
            if (Random.Range(0, 100) < percentObstacles)
            {
                GameObject obstacle = null;
                if (allBlocksByRow[i].Blocks[j].transform.childCount < 2)
                {
                    if (Random.Range(0, 100) < percentGems)
                    {
                        int typeOfgem = Random.Range(0, 100);
                        if (allBlocksByRow[i].Blocks[j].GetComponent<BlockId>().blockId != terrainBlocks[3].GetComponent<BlockId>().blockId)
                        {
                            if (typeOfgem < percentGem3)
                            {
                                obstacle = gems[2];
                            }
                            else if (typeOfgem<percentGem2)
                            {
                                obstacle = gems[1];
                            }
                            else
                            {
                                obstacle = gems[0];
                            } 
                        }
                    }
                    else
                    {
                        if (allBlocksByRow[i].Blocks[j].GetComponent<BlockId>().blockId == terrainBlocks[0].GetComponent<BlockId>().blockId)
                        {
                            obstacle = RockStone;
                        }
                        else if (allBlocksByRow[i].Blocks[j].GetComponent<BlockId>().blockId == terrainBlocks[1].GetComponent<BlockId>().blockId)
                        {
                            obstacle = trees[Random.Range(0, trees.Count)];
                        }
                        else if (allBlocksByRow[i].Blocks[j].GetComponent<BlockId>().blockId == terrainBlocks[2].GetComponent<BlockId>().blockId)
                        {

                        }
                    }
                }
                if (obstacle != null)
                {
                    GameObject newObstacle = Instantiate(obstacle, allBlocksByRow[i].Blocks[j].transform);
                    newObstacle.transform.SetSiblingIndex(0);
                    newObstacle.transform.position = new Vector2(firstPos.x + j, firstPos.y - i * 0.75f + 0.45f);
                    newObstacle.GetComponent<SpriteRenderer>().sortingOrder = 5 * i - 4;
                }
            }

        }
    }

    public void OrganizeBlocks()
    {
        for (int i = 0; i < allBlocksByRow.Count; i++)
        {
            for (int j = 0; j < allBlocksByRow[i].Blocks.Count; j++)
            {
                BlockId currentBlock = allBlocksByRow[i].Blocks[j].GetComponent<BlockId>();
                if ((i == 0 || currentBlock.blockId != allBlocksByRow[i - 1].Blocks[j].GetComponent<BlockId>().blockId) && (i == allBlocksByRow.Count - 1 || currentBlock.blockId != allBlocksByRow[i + 1].Blocks[j].GetComponent<BlockId>().blockId) && (j == 0 || currentBlock.blockId != allBlocksByRow[i].Blocks[j - 1].GetComponent<BlockId>().blockId) && (j == allBlocksByRow[i].Blocks.Count - 1 || currentBlock.blockId != allBlocksByRow[i].Blocks[j + 1].GetComponent<BlockId>().blockId))
                {
                    List<BlockId> nextBlocks = new List<BlockId>();
                    if (i != 0)
                    {
                        nextBlocks.Add(allBlocksByRow[i - 1].Blocks[j].GetComponent<BlockId>());
                    }
                    if (i != allBlocksByRow.Count - 1)
                    {
                        nextBlocks.Add(allBlocksByRow[i + 1].Blocks[j].GetComponent<BlockId>());
                    }
                    if (j != 0)
                    {
                        nextBlocks.Add(allBlocksByRow[i].Blocks[j - 1].GetComponent<BlockId>());
                    }
                    if (j != allBlocksByRow[i].Blocks.Count - 1)
                    {
                        nextBlocks.Add(allBlocksByRow[i].Blocks[j + 1].GetComponent<BlockId>());
                    }
                    int selectedBlock = Random.Range(0, nextBlocks.Count);
                    GameObject newTerrain = null;
                    for (int k = 0; k < terrainBlocks.Count; k++)
                    {
                        if (k == nextBlocks[selectedBlock].blockId)
                        {
                            newTerrain = terrainBlocks[k];

                            break;
                        }
                    }

                    Transform column = allBlocksByRow[i].Blocks[j].transform.parent;
                    GameObject newBlock = Instantiate(newTerrain, column.transform);
                    newBlock.name = newBlock.name + " Row " + i;
                    newBlock.transform.position = allBlocksByRow[i].Blocks[j].transform.position;
                    newBlock.GetComponent<SpriteRenderer>().sortingOrder = 5 * i - 5;
                    GameObject toDestroy = allBlocksByRow[i].Blocks[j];
                    allBlocksByRow[i].Blocks.RemoveAt(j);
                    allBlocksByRow[i].Blocks.Insert(j, newBlock);
                    Destroy(toDestroy);
                }
            }
        }
    }
    public void RemoveAll()
    {
        terrainGenerated = true;
        CreaturesSelection.creaturesSelectionInstance.ActivateStartButton();
        GameMatchManager.gameMatchManagerInstance.allCreaturesInScene.Clear();
        Destroy(GameObject.Find("Terrain"));

    }


    public void AsignCreaturePath(GameObject newCreature, GameObject blockByRow)
    {
        int row = -1;
        for (int i = 0; i < allBlocksByRow.Count; i++)
        {
            if (allBlocksByRow[i].Blocks.Contains(blockByRow))
            {
                row = i;
            }
        }

        GameMatchManager.gameMatchManagerInstance.allCreaturesInScene.Add(newCreature);
        newCreature.GetComponent<SpriteRenderer>().sortingOrder = (5 * row) - 1;
        newCreature.transform.GetChild(0).GetComponent<SpriteRenderer>().sortingOrder = (5 * row) - 1;
        newCreature.transform.GetChild(1).GetComponent<Canvas>().sortingOrder = (5 * row) - 1;
        CharactersControllers characterScript = newCreature.GetComponent<CharactersControllers>();

        for (int j = 0; j < allBlocksByRow[row].Blocks.Count; j++)
        {
            if (allBlocksByRow[row].Blocks[j].transform.childCount > 1)
            {
                if (allBlocksByRow[row].Blocks[j].transform.GetChild(allBlocksByRow[row].Blocks[j].transform.childCount - 1).GetComponent<BlockId>() != null)
                {
                    characterScript.path.Add(new CharactersControllers.TerrainBlock(allBlocksByRow[row].Blocks[j].transform.GetChild(allBlocksByRow[row].Blocks[j].transform.childCount - 1).GetComponent<BlockId>().blockId, allBlocksByRow[row].Blocks[j].transform.GetChild(allBlocksByRow[row].Blocks[j].transform.childCount - 1).GetComponent<BlockId>().nextPos.position));
                }
                else
                {
                    characterScript.path.Add(new CharactersControllers.TerrainBlock(allBlocksByRow[row].Blocks[j].GetComponent<BlockId>().blockId, allBlocksByRow[row].Blocks[j].GetComponent<BlockId>().nextPos.position, allBlocksByRow[row].Blocks[j].transform.GetChild(0).GetComponent<Obstacle>()));
                }
            }
            else
            {
                characterScript.path.Add(new CharactersControllers.TerrainBlock(allBlocksByRow[row].Blocks[j].GetComponent<BlockId>().blockId, allBlocksByRow[row].Blocks[j].GetComponent<BlockId>().nextPos.position));
            }
        }
    }

}

