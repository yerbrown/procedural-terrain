﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryAndCreatureManager : MonoBehaviour
{
    public static InventoryAndCreatureManager InventoryAndCreature;
    public Inventory myInventory;
    public MapPreset selectedMap;
    public List<CreatureStats> selectedCreatures = new List<CreatureStats>();

    public void Awake()
    {
        if (InventoryAndCreatureManager.InventoryAndCreature != null)
        {
            Destroy(gameObject);
        }
        else
        {
            foreach (CreatureStats creature in myInventory.allcreatures)
            {
                creature.RestartLvl();
            }
            myInventory.gem1 = -1;
            myInventory.gem2 = -1;
            myInventory.gem3 = -1;
            InventoryAndCreature = this;
        }
        DontDestroyOnLoad(this);
    }

}
