﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MapLoadData : MonoBehaviour
{
    public List<MapPreset> allMaps = new List<MapPreset>();
    public List<Button> allButtonsMap = new List<Button>();
    [SerializeField]
    private string loadedLastmap;
    public string LastMap
    {
        get { return loadedLastmap; }
        set
        {
            if (loadedLastmap == value)
            {
                return;
            }
            else
            {
                loadedLastmap = value;
                AssignMapToInventory();
                UnlockAllMymaps();

            }
        }
    }
    private void OnEnable()
    {
        if (InventoryAndCreatureManager.InventoryAndCreature.GetComponent<PlayersDataSave>().UserLastMapUnlocked != null)
        {
            if (InventoryAndCreatureManager.InventoryAndCreature.GetComponent<PlayersDataSave>().UserLastMapUnlocked == "")
            {
                LastMap = allMaps[0].name;
            }
            else
            {

                LastMap = InventoryAndCreatureManager.InventoryAndCreature.GetComponent<PlayersDataSave>().UserLastMapUnlocked;
            }
        }
    }

    public void UnlockAllMymaps()
    {
        ResetMaps();
        for (int i = 0; i < allMaps.Count; i++)
        {
            allMaps[i].unlocked = true;
            allButtonsMap[i].interactable = true;
            if (allMaps[i].name == LastMap)
            {
                return;
            }
        }
    }


    public void AssignMapToInventory()
    {
        InventoryAndCreatureManager.InventoryAndCreature.GetComponent<PlayersDataSave>().inventory.lastMap = LastMap;
    }
    private void ResetMaps()
    {
        for (int i = 0; i < allMaps.Count; i++)
        {
            if (i > 0)
            {
                allMaps[i].unlocked = false;

            }
        }
    }
}
