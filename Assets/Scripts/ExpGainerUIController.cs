﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExpGainerUIController : MonoBehaviour
{
    [System.Serializable]
    public class ExpWindow
    {
        public CreatureExperienceController creatureExp;

        public Image creatureSpr, expBar;
        public Text currentExp, expGained, lvlTxt;
    }

    public List<ExpWindow> allExpCreatures = new List<ExpWindow>();
    public GameObject passButton;
    public float timeExpGained = 4;
    public void PlusExp(ExpWindow expWindow)
    {
        if (expWindow.creatureExp.currentLvl >= 10)
        {
            expWindow.creatureExp.currentExp = 0;
            expWindow.creatureExp.expGained = 0;
        }
        else
        {
            if (expWindow.creatureExp.expGained > 0)
            {
                expWindow.creatureExp.currentExp++;
                expWindow.creatureExp.expGained--;
                if (expWindow.creatureExp.currentExp == expWindow.creatureExp.maxExp)
                {
                    if (expWindow.creatureExp.currentLvl < 10)
                    {
                        expWindow.creatureExp.currentLvl++;
                        expWindow.creatureExp.gameObject.GetComponent<CharactersControllers>().UpdateStats();
                    }
                    expWindow.creatureExp.currentExp = 0;
                    if (expWindow.creatureExp.expGained == 0)
                    {

                    }
                }
            }
        }
        expWindow.creatureSpr.sprite = expWindow.creatureExp.gameObject.GetComponent<CharactersControllers>().creatureInfo.creatureImg;
        expWindow.lvlTxt.text = expWindow.creatureExp.gameObject.name + " - Lvl " + expWindow.creatureExp.currentLvl.ToString("00");
        expWindow.expBar.fillAmount = (float)expWindow.creatureExp.currentExp / expWindow.creatureExp.maxExp;
        expWindow.currentExp.text = expWindow.creatureExp.currentExp.ToString("000") + "/" + expWindow.creatureExp.maxExp.ToString("000") + " Exp";
        expWindow.expGained.text = "+ " + expWindow.creatureExp.expGained;
    }
    private void Start()
    {
        for (int i = 0; i < allExpCreatures.Count; i++)
        {
            allExpCreatures[i].creatureExp = CreaturesSelection.creaturesSelectionInstance.creaturesSelected[i].GetComponent<CreatureExperienceController>();
        }
        UpdateAllExp();

    }
    private void Update()
    {
        if (CompareExpAlready())
        {
            UpdateAllExp();
        }


    }

    public bool CompareExpAlready()
    {
        for (int i = 0; i < allExpCreatures.Count; i++)
        {
            if (allExpCreatures[i] != null && allExpCreatures[i].creatureExp.expGained > 0)
            {
                return true;
            }
        }
        UpdateInventorycreatureStats();
        return false;
    }


    public void UpdateAllExp()
    {
        for (int i = 0; i < allExpCreatures.Count; i++)
        {
            PlusExp(allExpCreatures[i]);
        }
    }


    public void InterPolate(ExpWindow expWindow)
    {
        float expPerSecond = (float)expWindow.creatureExp.expGained / timeExpGained;
    }

    public void UpdateInventorycreatureStats()
    {
        for (int i = 0; i < allExpCreatures.Count; i++)
        {
            allExpCreatures[i].creatureExp.GetComponent<CharactersControllers>().creatureInfo.attack = allExpCreatures[i].creatureExp.GetComponent<CharactersControllers>().attack;
            allExpCreatures[i].creatureExp.GetComponent<CharactersControllers>().creatureInfo.lifeMax = allExpCreatures[i].creatureExp.GetComponent<CharactersControllers>().lifeMax;
            allExpCreatures[i].creatureExp.GetComponent<CharactersControllers>().creatureInfo.maxExpLvl = allExpCreatures[i].creatureExp.maxExp;
            allExpCreatures[i].creatureExp.GetComponent<CharactersControllers>().creatureInfo.currentExp = allExpCreatures[i].creatureExp.currentExp;
            allExpCreatures[i].creatureExp.GetComponent<CharactersControllers>().creatureInfo.level = allExpCreatures[i].creatureExp.currentLvl;


        }
        passButton.SetActive(true);
    }
}
