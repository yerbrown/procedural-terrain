﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(CreatureStats))]
[CanEditMultipleObjects]
public class CreatureStatsCustomEditor : Editor
{

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        CreatureStats variablesForScript = (CreatureStats)target;
        variablesForScript.evo = EditorGUILayout.IntSlider("Evo:", variablesForScript.evo, 1, 3);
        if (GUILayout.Button("Generate LvlStats"))
        {
            variablesForScript.GenerateLvlStats(variablesForScript.evo);
        }
        variablesForScript.lvlUp = EditorGUILayout.IntSlider("Lvl Up:", variablesForScript.lvlUp, 1, 10);
        if (GUILayout.Button("Change Level"))
        {
            variablesForScript.LevelUp(variablesForScript.lvlUp);
        }
        if (GUILayout.Button("Restart to Lvl 1"))
        {
            variablesForScript.RestartLvl();
        }
    }
}
