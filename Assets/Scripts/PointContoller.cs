﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PointContoller : MonoBehaviour
{
    public static PointContoller pointContollerInstance;
    public List<Text> itemPoints = new List<Text>();
    public List<int> points = new List<int>();
    private void Awake()
    {
        pointContollerInstance = this;
    }
    private void Start()
    {
        for (int i = 0; i < itemPoints.Count; i++)
        {
            points.Add(0);
            UpdatePoint(i);
        }
    }
    public void AddPoints(int itemType)
    {
        points[itemType]++;
        UpdatePoint(itemType);
    }

    public void UpdatePoint(int itemType)
    {
        itemPoints[itemType].text = points[itemType].ToString("00");
    }

    public void AddToInventory()
    {
        for (int i = 0; i < points.Count; i++)
        {
            if (i == 0)
            {
                InventoryAndCreatureManager.InventoryAndCreature.myInventory.gem1 += points[i];

            }
            else if (i == 1)
            {
                InventoryAndCreatureManager.InventoryAndCreature.myInventory.gem2 += points[i];
            }
            else if (i == 2)
            {
                InventoryAndCreatureManager.InventoryAndCreature.myInventory.gem3 += points[i];
            }
        }
        
    }
}
