﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreatureUIController : MonoBehaviour
{
    public GameObject lifeBar;
    public Image lifeImg;
    public Text leveltext;
    public CharactersControllers myCreature;
    public bool enemy;
    private void Start()
    {
        myCreature = GetComponent<CharactersControllers>();
        if (enemy)
        {
            Vector3 lifebarpos = lifeBar.transform.position;
            lifeBar.transform.position = new Vector3(lifebarpos.x * -1, lifebarpos.y, lifebarpos.z);
        }
    }
    public void UpdateUI()
    {
        lifeBar.SetActive(true);
        lifeImg.fillAmount = (float)myCreature.life / myCreature.lifeMax;
        leveltext.text = myCreature.expController.currentLvl.ToString();
    }

}

