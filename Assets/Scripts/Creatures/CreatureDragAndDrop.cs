﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatureDragAndDrop : MonoBehaviour
{
    public bool isDragging;

    public float grabOffset;

    public CharactersControllers criatureController;



    private void Awake()
    {
        criatureController = GetComponent<CharactersControllers>();
    }

#if UNITY_EDITOR
    private void OnMouseDown()
    {
        if (!criatureController.inPlay)
        {
            isDragging = true;
            criatureController.path.Clear();


            GetComponent<SpriteRenderer>().sortingOrder = 50;
            gameObject.layer = 2;
            GameMatchManager.gameMatchManagerInstance.allCreaturesInScene.Remove(gameObject);
        }
    }
#endif

    public void OnTouchDown()
    {
        if (!criatureController.inPlay)
        {
            isDragging = true;
            criatureController.path.Clear();


            GetComponent<SpriteRenderer>().sortingOrder = 50;
            gameObject.layer = 2;
            GameMatchManager.gameMatchManagerInstance.allCreaturesInScene.Remove(gameObject);

        }
    }

    public void OnTouchUp()
    {
        if (!criatureController.inPlay)
        {

            isDragging = false;

            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position), Vector2.zero);

            if (hit.collider != null)
            {
                if (hit.collider.CompareTag("Block") && CreaturesSelection.creaturesSelectionInstance.CompareCreatureInBlock(hit.collider.GetComponent<BlockId>().nextPos.position))
                {
                    GenerateTerrainManager.generateTerrainManagerInstance.AsignCreaturePath(gameObject, hit.collider.gameObject);
                    criatureController.inGround = true;
            
                }
                else
                {
                    CreaturesSelection.creaturesSelectionInstance.RestartcreaturePos(gameObject);
                }
            }
            else
            {
                CreaturesSelection.creaturesSelectionInstance.RestartcreaturePos(gameObject);
            }

            CreaturesSelection.creaturesSelectionInstance.ActivateStartButton();
            gameObject.layer = 0;
        }
    }

#if UNITY_EDITOR
    private void OnMouseUp()
    {
        if (!criatureController.inPlay)
        {

            isDragging = false;

            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit.collider != null)
            {
                if (hit.collider.CompareTag("Block") && CreaturesSelection.creaturesSelectionInstance.CompareCreatureInBlock(hit.collider.GetComponent<BlockId>().nextPos.position))
                {
                    GenerateTerrainManager.generateTerrainManagerInstance.AsignCreaturePath(gameObject, hit.collider.gameObject);
                    criatureController.inGround = true;
                }
                else
                {
                    CreaturesSelection.creaturesSelectionInstance.RestartcreaturePos(gameObject);
                }
            }
            else
            {
                CreaturesSelection.creaturesSelectionInstance.RestartcreaturePos(gameObject);
            }

            CreaturesSelection.creaturesSelectionInstance.ActivateStartButton();
            gameObject.layer = 0;
        }
    }
#endif

#if UNITY_IOS
    public void IOSDrag()
    {
        if (!criatureController.inPlay)
        {

            if (isDragging)
            {
                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position), Vector2.zero);

                if (hit.collider != null)
                {
                    if (hit.collider.CompareTag("Block"))
                    {
                        if (CreaturesSelection.creaturesSelectionInstance.CompareCreatureInBlock(hit.collider.GetComponent<BlockId>().nextPos.position))
                        {
                            transform.position = hit.collider.gameObject.transform.position + Vector3.down * 0.3f - Vector3.forward;
                            return;
                        }
                    }
                }
                Vector2 mousePositionxy = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position) - transform.position + Vector3.down * grabOffset;
                transform.Translate(mousePositionxy);
            }
        }
    }
#endif

    public void WindDrag()
    {
        if (!criatureController.inPlay)
        {

            if (isDragging)
            {
                RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

                if (hit.collider != null)
                {
                    if (hit.collider.CompareTag("Block"))
                    {
                        if (CreaturesSelection.creaturesSelectionInstance.CompareCreatureInBlock(hit.collider.GetComponent<BlockId>().nextPos.position))
                        {
                            transform.position = hit.collider.gameObject.transform.position + Vector3.down * 0.3f - Vector3.forward;
                            return;
                        }
                    }
                }
                Vector2 mousePositionxy = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position + Vector3.down * grabOffset;
                transform.Translate(mousePositionxy);
            }
        }
    }
    private void Update()
    {
#if UNITY_IOS
IOSDrag();
#elif UNITY_EDITOR
        WindDrag();
#endif


    }
}
