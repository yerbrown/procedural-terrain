﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;

[CreateAssetMenu(fileName = "Creature", menuName = "ScriptableObjects/CreatureScriptableObject", order = 1)]
public class CreatureStats : ScriptableObject
{
    [System.Serializable]
    public class LvlStats
    {
        public int lvl;
        public int damage;
        public int lifemax;
        public int maxExp;
        public int expGained;

        public LvlStats(int level, int evo)
        {
            lvl = level + 1;
            damage = 5 * evo + lvl * evo;
            lifemax = 5 * evo + 2 * lvl * evo;
            maxExp = 50 * evo * lvl * evo;
            expGained = 10 * evo + 5 * lvl * evo;

        }

    }

    public string keyID;
    public string creatureName;
    public Sprite creatureImg, creaturePlayImg, creatureMenuImg;

    public int attack;
    public float attackRate;
    public int lifeMax;
    public float moveSpeed;
    public int currentExp;
    public int maxExpLvl;

    public Types.CreatureTypes type;

    public AnimatorController animController;
    public AnimatorController attackEffectController;
    public int recruitSuccess;

    public List<LvlStats> lvlStats = new List<LvlStats>();
    public int level = 1;
    [HideInInspector]
    public int lvlUp;
    public bool unlocked;
    public CreatureStats evolution, preEvolution;
    [HideInInspector]
    public int evo;
    public int evoGem1, evoGem2, evoGem3;

    public void GenerateLvlStats(int level)
    {
        lvlStats.Clear();
        for (int i = 0; i < 10; i++)
        {
            lvlStats.Add(new LvlStats(i, level));
        }
    }

    public void RestartLvl()
    {
        level = 1;
        attack = lvlStats[0].damage;
        lifeMax = lvlStats[0].lifemax;
        maxExpLvl = lvlStats[0].maxExp;
        currentExp = 0;
        if (keyID != "01" && keyID != "04" && keyID != "07")
        {            
            unlocked = false;
        }
        else
        {
            level = 5;
            attack = lvlStats[4].damage;
            lifeMax = lvlStats[4].lifemax;
            maxExpLvl = lvlStats[4].maxExp;
        }
    }

    public void LevelUp(int lvl)
    {
        if (lvl > 0 && lvl < 11)
        {
            level = lvl;
            attack = lvlStats[level - 1].damage;
            lifeMax = lvlStats[level - 1].lifemax;
            maxExpLvl = lvlStats[level - 1].maxExp;
            currentExp = 0;
        }
    }
}
