﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Types
{
    public enum CreatureTypes
    {
        FIRE,
        WATER,
        GRASS,
        BUG,
        WIND
    }
    public class TypesStrongWeaks
    {
        public CreatureTypes type;
        public CreatureTypes typeWeak;
        public CreatureTypes typeStrong;

        public TypesStrongWeaks(CreatureTypes Type, CreatureTypes TypeWeak, CreatureTypes TypeStrong)
        {
            type = Type;
            typeWeak = TypeWeak;
            typeStrong = TypeStrong;
        }
    }

    public static List<TypesStrongWeaks> allTypes = new List<TypesStrongWeaks>()
    {
        new TypesStrongWeaks(CreatureTypes.FIRE, CreatureTypes.WATER, CreatureTypes.GRASS),
        new TypesStrongWeaks(CreatureTypes.WATER, CreatureTypes.GRASS, CreatureTypes.FIRE),
        new TypesStrongWeaks(CreatureTypes.GRASS, CreatureTypes.FIRE, CreatureTypes.WATER),
        new TypesStrongWeaks(CreatureTypes.BUG, CreatureTypes.WIND, CreatureTypes.GRASS),
        new TypesStrongWeaks(CreatureTypes.WIND, CreatureTypes.WATER, CreatureTypes.BUG)
    };


    public static float WeakOfType(CreatureTypes myType, CreatureTypes enemyType)
    {
        for (int i = 0; i < allTypes.Count; i++)
        {
            if (myType == allTypes[i].type)
            {
                if (allTypes[i].typeWeak == enemyType)
                {
                    return 0.5f;
                }
                else if(allTypes[i].typeStrong == enemyType)
                {
                    return 2;
                }
                else
                {
                    return 1;
                }
            }
        }
        return 1;
    }
}
