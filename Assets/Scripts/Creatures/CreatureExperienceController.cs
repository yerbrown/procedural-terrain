﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreatureExperienceController : MonoBehaviour
{
    public int currentLvl = 1;
    public int currentExp;
    public int maxExp;

    public int expGained;

    public void GainExp(int exp)
    {
        expGained += exp;
    }

    public int CalculateExpGainedByOtherCreatures(CharactersControllers creature)
    {
        for (int i = 0; i < creature.creatureInfo.lvlStats.Count; i++)
        {
            if (creature.creatureInfo.lvlStats[i].lvl == currentLvl)
            {
                return creature.creatureInfo.lvlStats[i].expGained;
            }
        }
        return 0;
    }
}
