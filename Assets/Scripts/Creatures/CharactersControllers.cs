﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Animations;
using UnityEngine;

public class CharactersControllers : MonoBehaviour
{
    public Vector3 nextPos;
    public float speed;

    public float laserLength;
    [System.Serializable]
    public class TerrainBlock
    {
        public int blockId;
        public Vector3 nextPos;
        public Obstacle obstacle;

        public TerrainBlock(int id, Vector3 pos, Obstacle obs = null)
        {
            blockId = id;
            nextPos = pos;
            obstacle = obs;
        }
    }

    public float combatCount, combatRate;
    public int attack = 1;
    public int life, lifeMax;
    public List<TerrainBlock> path = new List<TerrainBlock>();
    public int currentPoint;
    public bool completedPath = false;

    public Animator anim;

    public GameObject attackEffect;

    public bool inPlay = false;
    public bool inGround = false;
    public bool enemy = false;


    public CharactersControllers combatTarget;
    public CreatureUIController uIController;
    public CreatureStats creatureInfo;
    public CreatureExperienceController expController;

    private void Awake()
    {
        combatCount = combatRate;
        anim = GetComponent<Animator>();
        uIController = GetComponent<CreatureUIController>();
        expController = GetComponent<CreatureExperienceController>();
    }
    private void Start()
    {
        if (creatureInfo != null)
        {
            UpdateStats();
        }
    }


    public void UpdateStats()
    {
        gameObject.name = creatureInfo.creatureName;
        speed = creatureInfo.moveSpeed;
        combatRate = creatureInfo.attackRate;
        GetComponent<SpriteRenderer>().sprite = creatureInfo.creaturePlayImg;
        anim.runtimeAnimatorController = creatureInfo.animController;
        for (int i = 0; i < creatureInfo.lvlStats.Count; i++)
        {
            if (creatureInfo.lvlStats[i].lvl == expController.currentLvl)
            {
                attack = creatureInfo.lvlStats[i].damage;
                lifeMax = creatureInfo.lvlStats[i].lifemax;
                expController.maxExp = creatureInfo.lvlStats[i].maxExp;
            }
        }
        life = lifeMax;
        GetComponent<SpriteRenderer>().sprite = creatureInfo.creaturePlayImg;
    }
    // Update is called once per frame
    void Update()
    {
        CreatureMovementController();
    }


    public void CreatureMovementController()
    {
        if (inPlay && path.Count > 1)
        {
            if (currentPoint < path.Count)
            {
                if (combatCount <= combatRate + 1)
                {
                    combatCount += Time.deltaTime;
                }
                if (combatTarget != null)
                {
                    CombatSystem();
                }
                else
                {
                    if (!enemy)
                    {
                        if (path[currentPoint].obstacle != null)
                        {
                            ObstacleDestroySystem();
                        }
                        else
                        {
                            DetectFloorType();
                        }

                    }
                    else
                    {
                        if (path[currentPoint].obstacle != null)
                        {
                            if (!path[currentPoint].obstacle.item)
                            {
                                
                            }
                        }
                        else
                        {
                            DetectFloorType();
                        }
                    }

                }

            }

            if (transform.position == path[currentPoint].nextPos)
            {
                if (path.Count - 1 > currentPoint)
                {
                    currentPoint++;
                }
                else
                {
                    anim.SetBool("Finish", true);
                    transform.GetChild(1).gameObject.SetActive(false);
                }
            }
        }
    }


    public void CombatSystem()
    {
        if (combatCount >= combatRate)
        {
            AttackAnim(combatTarget.gameObject);
            combatCount = 0;

            combatTarget.life -= (int)(attack * Types.WeakOfType(creatureInfo.type, combatTarget.creatureInfo.type));
            if (Types.WeakOfType(creatureInfo.type, combatTarget.creatureInfo.type) == 0.5f)
            {
                Debug.Log("El ataque de " + System.Enum.GetName(typeof(Types.CreatureTypes), creatureInfo.type) + " fue poco efectivo contra la criatura de tipo " + System.Enum.GetName(typeof(Types.CreatureTypes), combatTarget.creatureInfo.type));
            }
            else if (Types.WeakOfType(creatureInfo.type, combatTarget.creatureInfo.type) == 2)
            {
                Debug.Log("El ataque de " + System.Enum.GetName(typeof(Types.CreatureTypes), creatureInfo.type) + " fue muy efectivo contra la criatura de tipo " + System.Enum.GetName(typeof(Types.CreatureTypes), combatTarget.creatureInfo.type));
            }
            else
            {
                Debug.Log("El ataque de " + System.Enum.GetName(typeof(Types.CreatureTypes), creatureInfo.type) + " fue efectivo contra la criatura de tipo " + System.Enum.GetName(typeof(Types.CreatureTypes), combatTarget.creatureInfo.type));
            }

            combatTarget.uIController.UpdateUI();
            expController.GainExp(expController.CalculateExpGainedByOtherCreatures(combatTarget));
            if (combatTarget.life <= 0)
            {
                combatTarget.UnlockCreature();
                combatTarget.anim.SetTrigger("Dead");
                combatTarget.GetComponent<SpriteRenderer>().sprite = combatTarget.creatureInfo.creaturePlayImg;
                combatTarget.combatTarget = null;
                combatTarget.transform.GetChild(1).gameObject.SetActive(false);
                combatTarget.speed = 0;
                combatCount = combatRate;
                combatTarget = null;
            }
        }
    }


    public void ObstacleDestroySystem()
    {
        if (!path[currentPoint].obstacle.item)
        {
            if (combatCount >= combatRate)
            {
                AttackAnim(path[currentPoint].obstacle.gameObject);
                path[currentPoint].obstacle.ShowLife(attack);
                expController.GainExp(path[currentPoint].obstacle.expGained);
                combatCount = 0;
                if (path[currentPoint].obstacle.life <= 0)
                {
                    combatCount = combatRate;
                    Destroy(path[currentPoint].obstacle.gameObject);
                }
            }
        }
    }

    public void DetectFloorType()
    {
        float currentSpeed = speed;
        if (creatureInfo.type != Types.CreatureTypes.WATER && creatureInfo.type != Types.CreatureTypes.WIND && path[currentPoint].blockId == 3)
        {
            currentSpeed = speed / 2;
        }

        transform.position = Vector3.MoveTowards(transform.position, path[currentPoint].nextPos, currentSpeed * Time.deltaTime);
    }

    public void UnlockCreature()
    {
        if (creatureInfo.unlocked != true)
        {
            if (Random.Range(0, 100) < creatureInfo.recruitSuccess)
            {
                creatureInfo.unlocked = true;
                InventoryAndCreatureManager.InventoryAndCreature.GetComponent<CreatureData>().UpdateMyCreaturesList(creatureInfo);
                InventoryAndCreatureManager.InventoryAndCreature.GetComponent<PlayersDataSave>().UpdateCreaturesInUser(InventoryAndCreatureManager.InventoryAndCreature.GetComponent<CreatureData>().myCreatures);
                GameMatchManager.gameMatchManagerInstance.ActivateUnlockWindow(creatureInfo.creatureImg);
            }
        }

    }

    public void FinishLevel()
    {
        if (!enemy)
        {
            completedPath = true;
            GameMatchManager.gameMatchManagerInstance.FinishLevel();
        }
    }


    public void AttackAnim(GameObject target)
    {
        anim.SetTrigger("Attack");
        GameObject attackEff = Instantiate(attackEffect);
        attackEff.GetComponent<Animator>().runtimeAnimatorController = creatureInfo.attackEffectController;
        if (enemy)
        {
            attackEff.GetComponent<SpriteRenderer>().flipX = true;
        }
        attackEff.GetComponent<SpriteRenderer>().sortingOrder = target.GetComponent<SpriteRenderer>().sortingOrder + 1;
        if (attackEff.transform.childCount > 0)
        {
            attackEff.transform.GetComponentInChildren<SpriteRenderer>().sortingOrder = target.GetComponent<SpriteRenderer>().sortingOrder + 2;
        }
        if (combatTarget != null)
        {
            attackEff.transform.position = target.transform.position + Vector3.up * 0.5f;
        }
        else
        {

            attackEff.transform.position = target.transform.position;
        }
        Destroy(attackEff, 0.5f);
    }

    public void Die()
    {
        if (enemy)
        {
            GenerateEnemies.generateEnemiesInstance.RemoveFromCurrentScene(gameObject);
        }
        else
        {
            GameMatchManager.gameMatchManagerInstance.RemoveCreatureFromCreaturesInScene(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (inPlay)
        {
            if (collision.CompareTag("Obstacle") && path[currentPoint + 1].obstacle != null && collision.GetComponent<Obstacle>() == path[currentPoint + 1].obstacle)
            {
                if (enemy)
                {
                    currentPoint += 2;
                }
                else
                {
                    PointContoller.pointContollerInstance.AddPoints(collision.GetComponent<Obstacle>().itemType);
                    expController.GainExp(collision.GetComponent<Obstacle>().expGained);
                    Destroy(collision.gameObject);
                }
            }

            if (collision.CompareTag("Creature"))
            {
                if (enemy)
                {
                    if (collision.GetComponent<CharactersControllers>().path[0].nextPos.y == path[path.Count - 1].nextPos.y)
                    {
                        combatTarget = collision.GetComponent<CharactersControllers>();
                    }
                }
                else
                {
                    if (collision.GetComponent<CharactersControllers>().path[path.Count - 1].nextPos.y == path[0].nextPos.y)
                    {
                        combatTarget = collision.GetComponent<CharactersControllers>();
                    }
                }
            }
        }
    }


}

