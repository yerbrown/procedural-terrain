﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameMatchManager : MonoBehaviour
{
    public static GameMatchManager gameMatchManagerInstance;

    public List<GameObject> allCreaturesInScene = new List<GameObject>();
    public GameObject passLevelWindow;
    public Animator transAnim;

    public GameObject pauseMenu, recruitWindow;
    public bool win = true;
    public int roundsCompleted;

    private void Awake()
    {
        gameMatchManagerInstance = this;

        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 60;
    }

    public void PassLevel()
    {
        Time.timeScale = 1;
        SaveData();
        if (roundsCompleted > 1)
        {
            PointContoller.pointContollerInstance.AddToInventory();

            InventoryAndCreatureManager.InventoryAndCreature.myInventory.CompareIfUnlockedIsPreviosLastMap(InventoryAndCreatureManager.InventoryAndCreature.selectedMap.nextMap);

            InventoryAndCreatureManager.InventoryAndCreature.GetComponent<CreatureData>().dataSaveController.SaveData();
            InventoryAndCreatureManager.InventoryAndCreature.selectedMap.nextMap.unlocked = true;
            TransitionToMenu();
        }
        else
        {
            if (win)
            {
                GenerateTerrainManager.generateTerrainManagerInstance.RemoveAll();
                CreaturesSelection.creaturesSelectionInstance.RestarAllCreaturesPos();
                GenerateTerrainManager.generateTerrainManagerInstance.GenerateRowBlocks();
                GenerateEnemies.generateEnemiesInstance.GenerateEnemiesInRows();
                roundsCompleted++;
            }
            else
            {
                PointContoller.pointContollerInstance.AddToInventory();
                InventoryAndCreatureManager.InventoryAndCreature.GetComponent<CreatureData>().dataSaveController.SaveData();
                TransitionToMenu();
            }
        }
    }

    public bool CompareLevelPass()
    {
        for (int i = 0; i < allCreaturesInScene.Count; i++)
        {
            if (!allCreaturesInScene[i].GetComponent<CharactersControllers>().completedPath)
            {
                return false;
            }
        }
        return true;
    }

    public void RemoveCreatureFromCreaturesInScene(GameObject creature)
    {
        creature.transform.GetChild(1).gameObject.SetActive(false);
        allCreaturesInScene.Remove(creature);
        creature.SetActive(false);
        FinishLevel();
    }
    public void FinishLevel()
    {
        if (allCreaturesInScene.Count == 0)
        {
            passLevelWindow.SetActive(true);
            Time.timeScale = 0;
            passLevelWindow.transform.GetChild(0).GetComponent<Text>().text = "Lose ...";
            win = false;
        }
        else
        {
            if (CompareLevelPass())
            {
                passLevelWindow.SetActive(true);
                Time.timeScale = 0;
                passLevelWindow.transform.GetChild(0).GetComponent<Text>().text = "Win !!";
                win = true;
            }

        }
    }

    public void SaveData()
    {

        for (int i = 0; i < InventoryAndCreatureManager.InventoryAndCreature.selectedCreatures.Count; i++)
        {
            InventoryAndCreatureManager.InventoryAndCreature.GetComponent<CreatureData>().UpdateMyCreaturesList(InventoryAndCreatureManager.InventoryAndCreature.selectedCreatures[i]);
        }
        InventoryAndCreatureManager.InventoryAndCreature.GetComponent<CreatureData>().dataSaveController.UpdateCreaturesInUser(InventoryAndCreatureManager.InventoryAndCreature.GetComponent<CreatureData>().myCreatures);
    }
    public void StartMatch()
    {
        CreaturesSelection.creaturesSelectionInstance.StartPlay();
        GenerateEnemies.generateEnemiesInstance.StartPlay();
    }


    public void Restart()
    {
        PointContoller.pointContollerInstance.AddToInventory();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void BackToMenu()
    {
        Time.timeScale = 1;
        SaveData();
        SceneManager.LoadScene(0);
    }

    public void TransitionToMenu()
    {
        Time.timeScale = 1;
        transAnim.SetTrigger("Pass");
        Invoke(nameof(BackToMenu), 1);
    }

    public void PauseGame()
    {
        Time.timeScale = 0;
        pauseMenu.SetActive(true);

    }

    public void ReturnGame()
    {
        Time.timeScale = 1;
        pauseMenu.SetActive(false);
    }

    public void ActivateUnlockWindow(Sprite creatureIcon)
    {
        recruitWindow.SetActive(false);
        recruitWindow.transform.GetChild(0).GetComponent<Image>().sprite = creatureIcon;
        recruitWindow.SetActive(true);
    }
}
