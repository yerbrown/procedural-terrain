﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Creature", menuName = "ScriptableObjects/InventoryScriptableObject", order = 2)]
public class Inventory : ScriptableObject
{
    public int gem1, gem2, gem3;
    public string lastMap;

    public List<CreatureStats> allcreatures = new List<CreatureStats>();
    public List<MapPreset> allMaps = new List<MapPreset>();

    public void CompareIfUnlockedIsPreviosLastMap(MapPreset map)
    {
        for (int i = 0; i < allMaps.Count; i++)
        {
            if (allMaps[i] == map)
            {
                for (int j = 0; j < allMaps.Count; j++)
                {
                    if (allMaps[j].name == lastMap)
                    {
                        if (i > j )
                        {
                           lastMap = map.name;
                        }
                    }
                }
                
            }
        }
    }
}
